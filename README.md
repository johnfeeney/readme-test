# demo-java-examples

A collection of Java demo examples.

To compile/build/package all:

```
mvn clean install
```

## a-spring-boot-web

This module is a Spring Boot Web project. To run the web app:

```
./mvnw spring-boot:run -pl a-spring-boot-web
```

Serves the following URLs:

[http://localhost:8080](http://localhost:8080)

[http://localhost:8080/login](http://localhost:8080/login)

## b-code-completion-example

Use Student.java to:
1. Create code using comment, e.g., 

> _// create constructor with firstName, lastName, dateOfBirth, emailAddress_

2. Demonstrate completion of subsequent boilerplate code: variables, getters and setters, etc.

## c-code-quality-example

TBC

## d-unit-tests-example

Highlight the entire `setDateOfBirth` method in the `Person` class and, using Chat, generate unit tests for the various checks within the method

## e-documentation-example

TBC

## f-context-interface-fim-example

#### Context example 1
Demonstrate context obtained from within a file by add the following comment: 
> _// test dog using interface_

In the subsequent generated code, observe that the variables remain prefixed with `_`.

#### Context example 2

Demonstrate context onbtained from another file open in the IDE:

1. Open the `example.intrface.I_Animal` class and uncomment the `introducesItself` method
2. Open the `example.implementation.Cat` class and show how a new code suggestion offers to add this very same method.
3. Modify the method so that it look as follows:
```
    public void introducesItself(String name) {
        System.out.println("My name is " + name);
    }
```

#### Fill-in-the-middle example

1. In the `I_Animal` class, add age as an additional parameter to the method:
```
    void introducesItself(String name, int age);
```
2. In the Cat class, add a comma (`,`) after the last parameter in the following method:
```
    public void introducesItself(String name) {
```
3. Observe that Tabnine offers the suggestion of an additional parameter ( `int age`) here.

4. Seek a suggestion at the end of this line:
```
    System.out.println("My name is " + name);
```
5.  Tabnine should offer a similar suggestion to the following:
```
System.out.println("My name is " + name + " and I am " + age + " years old.");
```

#### Fill-in-the-middle example 2

1. In the `I_Animal` class, change the age parameter to be of type String:
```
    void introducesItself(String name, String age);
```
2. Return to the Cat class and remove `int age` from the following method signature:
```
    public void introducesItself(String name, int age) {
```
3. Prompt for a new suggestion in place of the parameter you just removed and note that Tabnine now offers `String age`

## g-explain-this-code

TBC

## h-chat-generation

Demonstrate collaboration with Chat and iteration towards a solution using a CSV parsing example.

- CSV file located at `src/test/resources/devices.csv`
- Prompts located at `src/test/resources/prompt.txt`



